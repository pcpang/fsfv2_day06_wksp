/**
 * Created by pohchye on 4/7/2016.
 */

var ValidateApp = angular.module("ValidateApp", []);

(function () {
    var ValidateCtrl;
    ValidateCtrl = function($http) {
        var ctrl = this;
        ctrl.firstName = "";
        ctrl.lastName = "";
        ctrl.password = "";
        ctrl.status = {
            message: "",
            code: 0
        };
        ctrl.register = function () {
          $http.post("/register", {
              params: {
                  firstName: ctrl.firstName,
                  lastName: ctrl.lastName,
                  password: ctrl.password
              }
          }).then(function () {
              console.info("Success");
              ctrl.status.message="Your registration is complete";
              ctrl.status.code= 200;
          }).catch(function () {
              console.info("Failure");
              ctrl.status.message="Your registration failed";
              ctrl.status.code= 400;
          });
           
        };
    };
    
    ValidateApp.controller("ValidateCtrl", ["$http", "$window", ValidateCtrl]);

})();