/**
 * Created by pohchye on 4/7/2016.
 */

// Create the Express app
var express=require("express");
var app=express();

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

// Serve register resource
app.post("/register", function (req, res) {
    var firstName = req.body.params.firstName;
    var lastName = req.body.params.lastName;
    var password = req.body.params.password;
    console.info("First Name: %s", firstName);
    console.info("Last Name: %s", lastName);
    console.info("Postal Code: %s", password);
    res.status(200).end();
});

// Serve public files
app.use(express.static(__dirname + "/public"));

app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3007
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});